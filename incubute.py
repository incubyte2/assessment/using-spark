#!/usr/bin/python3
from pyspark.sql import SparkSession
from pyspark.sql.functions import *

# create SparkSession object
spark = SparkSession.builder\
            .appName("patients")\
            .enableHiveSupport()\
            .getOrCreate()

patients= spark.read\
            .option("header", "true")\
            .option("inferSchema", "true")\
            .option("nullValue", "NULL")\
            .option("mode", "DROPMALFORMED")\
            .option("delimiter", '|')\
            .csv("/home/dnyaneshwar/Desktop/49636_dnyaneshwar/incubyte/customer.csv")

patients.show(truncate=False)
patients = patients.drop('H','_c0')

patients.write\
    .partitionBy("Country")\
    .saveAsTable("cust_country_part", format="orc", mode="OVERWRITE")

print("cust_country_part table written.")
patients.show(truncate=False)


query = input("Enter query to get information of customers of perticular country ")
result = spark.sql(query)
# "Select * from cust_country_part where Country='IND';
# Select * from cust_country_part where Country='USA';
# Select * from cust_country_part where Country='PHIL';
# Select * from cust_country_part where Country='NYC';
# Select * from cust_country_part where Country='AU';"
result.show(truncate=False)
spark.stop()

